﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstGIT
{
    class Program
    {
        private static void Sum(int a, int b)
        {
            int S = 0;
            S = a + b;
            Console.WriteLine("The sum of numbers is: " + S);
        }

        private static void Product(int a, int b)
        {
            int P = 0;
            P = a * b;
            Console.WriteLine("The product of numbers is: " + P);
        }

        private static void Maximum(int[] a)
        {
            int max = -999;
            for(int i=0; i<a.Length; i++)
            {
                if (a[i] > max)
                    max = a[i];
            }
            Console.WriteLine("The maximum element from the array is: " + max);
        }

        private static void Minimum(int[] a)
        {
            int min = 999;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] < min)
                    min = a[i];
            }
            Console.WriteLine("The minimum element from the array is: " + min);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("First git project!");
            Console.WriteLine("Testing commits to see how they work!");
            Sum(7, 13);
            Product(7, 13);

            int[] A = { 1, 7, 5, 13, 24, 11 };
            Maximum(A);
            Minimum(A);
        }
    }
}
